const TeleBot = require('telebot');
const infos = require('./infos.json')
const bot = new TeleBot(infos.botToken);
const fs = require('fs');
const request = require('request');
const admins = [TELEGRAM IDs of admins];
let keywords = require('./keywords.json');

// API call
function fetchKeyboards() {
  var url = "http://svcs.ebay.com/services/search/FindingService/v1";
      url += "?OPERATION-NAME=findItemsAdvanced";
      url += "&SERVICE-VERSION=1.0.0";
      url += "&SECURITY-APPNAME=" + infos.ebayToken;
      url += "&RESPONSE-DATA-FORMAT=JSON";
      url += "&callback=reponse";
      url += "&REST-PAYLOAD";
      url += "&descriptionSearch=false";
      url += "&categoryId(0)=11189";
	  url += "&categoryId(1)=3676";
      url += "&keywords=" + keywords.keywords;
      url += "&sortOrder=StartTimeNewest";
      url += "&paginationInput.entriesPerPage=10";
      url += "&itemFilter.name=LocatedIn";
      url += "&itemFilter.value=WorldWide";


      request(url, function (error, response, pageContent) {
        if (error) {
			var now 	= new Date(); 
			var year   = now.getFullYear();
			var month    = now.getMonth() + 1;
			var day    = now.getDate();
			var hour   = now.getHours();
			var minute  = now.getMinutes();
			var second = now.getSeconds();
			var dd 		= day+"/"+month+"/"+year+"-- "+hour+"-"+minute+"-"+second+" :: ";
            console.log(dd, error, response);
        }
	if (typeof pageContent == "string"){ // prevent the bot from crashing if the type is undefined
		let res = JSON.parse(pageContent.substr(12).slice(0, -1));
		let items = res.findItemsAdvancedResponse[0].searchResult[0].item;
		for (var i = 0; i < items.length; i++) {
			var keyboard = items[i];
			var id = keyboard.itemId[0];
			var title = keyboard.title[0];
			var country = keyboard.country[0];
			var price = keyboard.sellingStatus[0].currentPrice[0].__value__;
			var currency = keyboard.sellingStatus[0].currentPrice[0]["@currencyId"];
			var url = keyboard.viewItemURL[0];
			var cat = keyboard.primaryCategory[0].categoryName[0];
			var lType = keyboard.listingInfo[0].listingType[0];
			var bo = keyboard.listingInfo[0].bestOfferEnabled[0];
			keyboardCheck(id, lType, bo, keyboard, title, country, price, currency, url, cat);
			}
		}
      })
}

//check if the keyboard has already been sent
function keyboardCheck(id, lType, bo, keyboard, title, country, price, currency, url, cat) { 
  fs.readFile('ebayIds.txt', function (err, data) {
	var now 	= new Date(); 
	var year   = now.getFullYear();
	var month    = now.getMonth() + 1;
	var day    = now.getDate();
	var hour   = now.getHours();
	var minute  = now.getMinutes();
	var second = now.getSeconds();
	var dd 		= day+"/"+month+"/"+year+"-- "+hour+"-"+minute+"-"+second+" :: ";
    if (err) {
      console.log(dd,err);
    }
    if (data.indexOf(id) >= 0) { // the keyboard has already been sent
	  //var nouveaute = 0;
      //console.log(dd, nouveaute, "pas de nouveautes");
    } else { 
      var ids = fs.createWriteStream('ebayIds.txt', {flags: 'a'});
      ids.write(id + "\n");
      ids.end();
      let type = listingType(lType, bo, keyboard);
	  url = urlShortener(url);
      sendMessage(title, country, price, currency, url, cat, type);
    }
  })
}


function listingType(lType, bo, keyboard) {
  if (lType == "AuctionWithBIN") {
    var bNPrice = keyboard.listingInfo[0].buyItNowPrice[0].__value__;
    var currency = keyboard.listingInfo[0].buyItNowPrice[0]["@currencyId"];
    var typeTemp = "[A]" + "[BN : " + bNPrice + currency + "]";
    return typeTemp;
  } else if (lType == "Auction") {
    var typeTemp = "[A]";
    return typeTemp;
  } else {
    if (bo == "true") {
      var typeTemp = "[BO]";
      return typeTemp;
    } else {
      var typeTemp = "";
      return typeTemp;
    }
  }
}

function urlShortener(url) {
	let startingPos = url.indexOf("/itm/") + 5;
	let endPos = url.indexOf("/", startingPos) + 1;
	let toBReplaced = url.substring(startingPos, endPos);
	return url.replace(toBReplaced, "");
}


function sendMessage(title, country, price, currency, url, cat, type) {
  let caption = "[" + country + "]" + type + "[" + price + currency + "] " + title + " " + url + " (" + cat + ")";
  return bot.sendPhoto(infos.chatId, url, {caption: caption});
}


bot.on('start', (msg) => {
  setInterval(fetchKeyboards, 20000);
})

bot.on([/^\/blacklist (.+)$/,/^\/bl (.+)$/], (msg, props) => {
	if (admins.includes(msg.from.id)) {
		let arg = props.match[1].split(" ")[0].toLowerCase();
		if (!keywords.keywords.split(" ").includes("-" + arg)) {
			keywords.keywords += " -" + arg;
			let bl = fs.createWriteStream('keywords.json');
			bl.write(JSON.stringify(keywords));
			bl.end();
		} else {
			return bot.sendMessage(msg.from.id, arg + " already added to blacklist");
		}
	} else {
		return bot.sendMessage(msg.from.id, "You're not authorized to modify the blacklist");
	}
})

bot.on([/^\/unblacklist (.+)$/,/^\/ubl (.+)$/], (msg, props) => {
	if (admins.includes(msg.from.id)) {
		let arg = props.match[1].split(" ")[0].toLowerCase();
		if (arg == "keyboard") {
			return bot.sendMessage(msg.from.id, "cannot remove keyboard");
		} else {
			let kwords = keywords.keywords.split(" ");
			let i = 1;
			let found = false;
		
			while (i < kwords.length && !found) {
				if (kwords[i] == "-" + arg) {
					kwords.splice(i, 1);
					keywords.keywords = "keyboard";
					found = true;
				}
				i++;
			}
		
			if (found) {
				for (let j = 1; j < kwords.length; j++) {
					keywords.keywords += " " + kwords[j];
				}
		
				let bl = fs.createWriteStream('keywords.json');
				bl.write(JSON.stringify(keywords));
				bl.end();
			} else {
				return bot.sendMessage(msg.from.id, arg + " not in the list");
			}
		

		}
	} else {
		return bot.sendMessage(msg.from.id, "You're not authorized to modify the blacklist");
	}

})

bot.start();
